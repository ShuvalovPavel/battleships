#include <iostream>
#include <locale.h>
#include <time.h> 
#include <conio.h>
#include <vector>
#include "Field.h"
#include "Player.h"

using namespace std;

class Game
{
private:
	int i_User, j_User, i_Pc, j_Pc;
	int Pc_c, User_c;
	int BotWin, HumanWin;
	int ShipStatus, i_Pc_Hit, j_Pc_Hit;
public:
	vector<int> ships;
	Game();
	~Game();
	void  Play();
	void  P(AnyField UserFieldForPc, AnyField & ShowUserField);
};

